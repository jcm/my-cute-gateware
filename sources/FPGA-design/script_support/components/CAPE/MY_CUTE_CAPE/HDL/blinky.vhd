library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity blinky is
    port (
        clk : in std_logic;
        resetn : in std_logic;
        blink: out std_logic
    );
end blinky;

architecture behavioral of blinky is
    signal counter : unsigned(22 downto 0);
begin
    blink <= counter(22);
    process(clk, resetn)
    begin
        if resetn = '0' then
            counter <= (others => '0');
        elsif rising_edge(clk) then
            counter <= counter + 1;
        end if;
    end process;
end architecture;
